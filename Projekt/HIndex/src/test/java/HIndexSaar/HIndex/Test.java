package HIndexSaar.HIndex;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class Test {

	@org.junit.Rule
	public TestRule watcher = new TestWatcher() {
		@Override
		protected void starting(Description description) {
			System.out.println("Starting test: " + description.getMethodName());
		}
	};

	private GSReader reader;

	@Before
	public void initialize(){
		reader = new GSReader();
	}

	//	@org.junit.Test
	//	public void dummyTest(){
	//		String data = "data/dblp.xml";
	//		String[] args = new String[1];
	//		args[0] = data;
	//		Parser.main(args);
	//		BufferedReader br;
	//		try {
	//			br = new BufferedReader(new FileReader("affil.txt"));
	//			assertTrue(br.readLine() == null);
	//		} catch (FileNotFoundException e) {
	//			// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		}catch (IOException e) {
	//			// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		}
	//	}

	@org.junit.Test
	public void ReaderTest(){
		PersonRequest request = new PersonRequest("https://docs.oracle.com/javase/tutorial/networking/urls/readingURL.html", "Holger Hermanns");
		assertTrue("ReaderTest", request.getResults().isEmpty());
		List<String> res = new ArrayList<String>();
		reader.readHTMLPerson(request, res );
		assertTrue(request.getResults().isEmpty());
	}

	@org.junit.Test
	public void ScholarTest(){
		PersonRequest request = new PersonRequest("cs.uni-saarland.de","Holger Hermanns");
		List<String> res = new ArrayList<String>();
		reader.readHTMLPerson(request, res );
		assertFalse("ScholarTest", res.isEmpty());
	}

	@org.junit.Test
	public void ReaderTestSpecific(){
		PersonRequest request = new PersonRequest("cs.uni-saarland.de","Holger Hermanns");
		List<String> results = new ArrayList<String>();
		reader.readHTMLPerson(request, results);
		assertFalse("ReaderTestSpecific", results.isEmpty());
	}

}
