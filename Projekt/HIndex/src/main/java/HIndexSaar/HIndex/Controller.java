package HIndexSaar.HIndex;

public class Controller {

	private Parser parser;
	private DBManager manager;

	public Controller(String xmlURL){
		// xmlURL is the path to the dblp.xml file
		setParser(new Parser(xmlURL));
		manager = new DBManager("jdbc:postgresql://localhost/HIndex");
	}

	public Parser getParser() {
		return parser;
	}

	public void setParser(Parser parser) {
		this.parser = parser;
	}

	public DBManager getManager() {
		return manager;
	}

	public void setManager(DBManager manager) {
		this.manager = manager;
	}
}
