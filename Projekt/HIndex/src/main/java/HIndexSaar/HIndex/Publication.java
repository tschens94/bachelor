package HIndexSaar.HIndex;
/*
 * Created on 07.06.2005
 */

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author ley
 *
 * created first in project xml5_coauthor_graph
 */
public class Publication {
	private static Set<Publication> ps= new HashSet<Publication>(650000);
	private static int maxNumberOfAuthors = 0;
	private String key;
	private Person[] authors;	// or editors

	public Publication(String k, Person[] persons) {
		setKey(k);
		authors = persons;
		ps.add(this);
		if (persons.length > maxNumberOfAuthors) {
			maxNumberOfAuthors = persons.length;
		}
	}

	public static int getNumberOfPublications() {
		return ps.size();
	}

	public static int getMaxNumberOfAuthors() {
		return maxNumberOfAuthors;
	}

	public Person[] getAuthors() {
		return authors;
	}

	static Iterator<Publication> iterator() {
		return ps.iterator();
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
