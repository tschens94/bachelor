package HIndexSaar.HIndex;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBManager {
	// JDBC Driver and database URL
	static final String JDBC_DRIVER = "org.postgresql.Driver";
	static final String DB_URL 		= "jdbc:postgresql://localhost/HIndex";

	// Database credentials
	static final String USER 		= "index_user";
	static final String PASSWORD 	= "password";

	private Connection conn;

	/**
	 * @param dburl: url to the postgresql database
	 */
	public DBManager(String dburl){
		// Register JDBC Driver
		try {
			Class.forName(JDBC_DRIVER);
			System.out.println("[INFO] Connecting to database...\r");
			conn = DriverManager.getConnection(DB_URL, USER, PASSWORD);
			System.out.println("[INFO] Connected to database!\r");
		} catch (ClassNotFoundException e) {
			System.err.println("[ERROR] Class" + JDBC_DRIVER + "not found.");
		} catch (SQLException e) {
			System.err.println("[ERROR] Error in initialising the database manager: "+ e);
		}
		try {
			PreparedStatement stmt = conn.prepareStatement("CREATE TABLE IF NOT EXISTS hindex ( "
					+ 	" affiliation character varying, "
					+ 	" name character varying PRIMARY KEY)");
			stmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Connection getConnection(){
		// Create local variables for executing statements
		return conn;
	}

	/**
	 * creates a SELECT-SQL Statement
	 * @param name: the name, that should be selected
	 * @return the results of the query
	 */
	public ResultSet createSQLStatement(String name) {
		PreparedStatement stmt;
		ResultSet result = null;
		try {
			stmt = getConnection().prepareStatement("SELECT exists( select count(*) from hindex where name=?)");
			stmt.setString(1, name);
			result = stmt.executeQuery();
		} catch (SQLException e) {
			System.err.println("[ERROR] Die Abfrage " + name + "lieferte einen Fehler:\n" + e );
		}
		return result;
	}

	public int updateSQLStatement(String column, String value){
		PreparedStatement stmt;
		int updatedColumns = 0;
		try {
			stmt = getConnection().prepareStatement("Update hindex set affiliation= ? WHERE name = ?");
			stmt.setString(1, value);
			stmt.setString(2, column);
			updatedColumns = stmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println("[ERROR] Die Abfrage " + column + "lieferte einen Fehler:\n" + e );
		}
		return updatedColumns;
	}

	/**
	 * @param name: the name of the professor that should be selected
	 * @param affil: its affiliation
	 * @return the number of updated columns
	 */
	public int insertSQLStatement(String name, String affil){
		PreparedStatement stmt;
		int updatedColumns = 0;
		try{
			stmt = getConnection().prepareStatement("INSERT INTO hindex VALUES (?,?)");
			stmt.setString(1, affil);
			stmt.setString(2, name);
			updatedColumns = stmt.executeUpdate();
		} catch (SQLException e){
			System.err.println("[ERROR] The insertion statement threw an exception: \n" + e );
			e.printStackTrace();
		}
		return updatedColumns;
	}

	public void runExample(){
		ResultSet result = null;
		insertSQLStatement("Holger Hermanns", "Uni Saarland");
		insertSQLStatement("Bernd Finkbeiner", "Uni Saarland");
		result = createSQLStatement("Holger Hermanns");
		int i = 0;
		try {
			while(result.next()) {
				//Closing the connection
				System.out.println("Column " + i + ":\n");
				System.out.println(result.getString(1) + "\n");
			}
			System.out.println("[INFO] Closing connection...");
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args){
		DBManager manager = new DBManager("jdbc:postgresql://localhost/HIndex");
		manager.runExample();
	}
}
