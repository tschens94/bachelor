package HIndexSaar.HIndex;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.List;

public class GSReader {

	/**
	 * @param request: 			The GS-request to find the affiliation of a person
	 * @param searchResults: 	The result list in format: [Name: Affiliation] (*CURRENTLY*)
	 * @return the searchResults
	 */
	@SuppressWarnings("finally")
	public List<String> readHTMLPerson(PersonRequest request, List<String> searchResults){
		try {
			Connection conn = Jsoup.connect(request.getUrl().toString());
			conn.timeout(0);
			Document doc = conn.get();
			Elements names = doc.select("div.gsc_1usr_text");
			String affiliation = "";
			for(Element m : names){ // by default, the first child should contain the name of the person
				Element child = m.child(0);
				if(child.text().equals(request.getSearchName())){
					affiliation = m.child(1).text();
				}
			}
			searchResults.add(request.getSearchName() + ": " + affiliation);
		} catch (IOException e) {

			e.printStackTrace();
		} finally {
			return searchResults;
		}
	}
}