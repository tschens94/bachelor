package HIndexSaar.HIndex;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

	public static void main(String[] args) throws IOException {
		if (args.length < 1) {
			System.out.println("Usage: java Parser [input]");
			System.exit(0);
		}
		// args[0] should contain the path to dblp.xml
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		System.out.println("[INFO] Start Parsing: " + sdf.format(now));
		Parser p = new Parser(args[0]);
		DBManager man = new DBManager("jdbc:postgresql://localhost/HIndex");
		ConfigHandler handler = new ConfigHandler(man);
		p.startParsing(args[0], handler );
		Date after = new Date();
		System.out.println("[INFO] End Parsing: " + sdf.format(after));
		System.out.println("[INFO] Number of parsed universities: " + handler.getUnis().size());
	}

}
