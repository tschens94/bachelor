package HIndexSaar.HIndex;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class MyFileWriter extends FileWriter {
	
	private String filename;

	public MyFileWriter(File file) throws IOException {
		super(file);
		this.filename = file.toString();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @param filename the filename to set
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

}
