package HIndexSaar.HIndex;

import java.util.ArrayList;

public class University {

	private String name;

	private ArrayList<Person> persons;

	public University(String n){
		name = n;
		persons = new ArrayList<Person>();
	}

	public ArrayList<Person> getPersons() {
		return persons;
	}

	public void setPersons(ArrayList<Person> persons) {
		this.persons = persons;
	}

	@Override
	public boolean equals(Object o){
		if(o instanceof University){
			University u = (University) o;
			return (u.name == this.name);
		}else if (o == this){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public int hashCode(){
		int result = 0;
		for(Character c : name.toCharArray()){
			result += (int) c;
		}
		return result;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
