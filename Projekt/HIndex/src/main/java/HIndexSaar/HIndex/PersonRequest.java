package HIndexSaar.HIndex;

public class PersonRequest extends ScholarRequest {

	private static final String BASIC_GS_URL = "https://scholar.google.com/citations?mauthors=";
	private static final String URL_APPENDIX = "&hl=de&view_op=search_authors";

	public PersonRequest(String url, String prof) {
		super(url, prof);
		buildURL();
	}

	private void buildURL() {
		setUrl(BASIC_GS_URL + getUrl() + URL_APPENDIX);
	}

}
