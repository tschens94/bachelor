package HIndexSaar.HIndex;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

public class ConfigHandler extends DefaultHandler {
	private DBManager dbmanager;

	private boolean hasUrl;
	private boolean firstQuery;

	private Set<University> unis;

	private Person currentPerson;
	private University tempUni;

	private boolean isAuthor = false;

	private String currentName;
	private StringBuffer buffer;

	ConfigHandler(DBManager manager) throws IOException{
		unis = new HashSet<University>();
		dbmanager = manager;
		firstQuery = true;
	}

	@Override
	public void startElement(String namespaceURI, String localName,
			String rawName, Attributes atts) throws SAXException {
		if(rawName.equals("url")){
			buffer = new StringBuffer();
		}
		if(rawName.equals("author")){
			buffer = new StringBuffer();
		}
		if(rawName.equals("title")){
			isAuthor = false;
		}
		if(rawName.equals("www")){
			buffer = new StringBuffer();
		}
	}

	@Override
	public void endElement(String namespaceURI, String localName,
			String rawName) throws SAXException {
		if(rawName.equals("author")){
			if(!isAuthor){
				currentName = buffer.toString();
				isAuthor = true;
			}
		}
		if(rawName.equals("url")){
			tempUni = new University(buffer.toString());
		}
		if(rawName.equals("www")){
			/*
			 * Set all fields of 'Person' and 'University' that have been collected in characters method.
			 */
			currentPerson = new Person(currentName);
			currentPerson.setAffiliation(tempUni);
			if(!firstQuery){
				ResultSet rs = dbmanager.createSQLStatement(currentName);
				try {
					if(!rs.next()){
						dbmanager.insertSQLStatement(currentName,currentPerson.getAffiliation().getName());
					}else{
						//TODO: update the already contained data by the new one.
						dbmanager.updateSQLStatement(currentName, tempUni.getName());
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				dbmanager.insertSQLStatement(currentName,currentPerson.getAffiliation().getName());
				firstQuery = false;
			}
			if(tempUni != null){ // tempUni == null if person does not have url field.
				tempUni.getPersons().add(currentPerson);
				if(!unis.add(tempUni)){// the university is already in the HashSet
					for(University u : unis){
						if(u.equals(tempUni)){
							u.getPersons().add(currentPerson);
						}
					}
				}
			}

		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		buffer.append(new String(ch, start, length));
	}

	private void Message(String mode, SAXParseException exception) {
		System.out.println(mode + " Line: " + exception.getLineNumber()
		+ " URI: " + exception.getSystemId() + "\n" + " Message: "
		+ exception.getMessage());
	}

	@Override
	public void warning(SAXParseException exception) throws SAXException {

		Message("**Parsing Warning**\n", exception);
		throw new SAXException("Warning encountered");
	}

	@Override
	public void error(SAXParseException exception) throws SAXException {

		Message("**Parsing Error**\n", exception);
		throw new SAXException("Error encountered");
	}

	@Override
	public void fatalError(SAXParseException exception) throws SAXException {

		Message("**Parsing Fatal Error**\n", exception);
		throw new SAXException("Fatal Error encountered");
	}
	public boolean isHasUrl() {
		return hasUrl;
	}
	public void setHasUrl(boolean hasUrl) {
		this.hasUrl = hasUrl;
	}

	/**
	 * @return the unis
	 */
	public Set<University> getUnis() {
		return unis;
	}

	/**
	 * @param unis the unis to set
	 */
	public void setUnis(Set<University> unis) {
		this.unis = unis;
	}
}