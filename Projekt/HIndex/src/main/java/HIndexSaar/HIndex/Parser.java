package HIndexSaar.HIndex;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * @author Jens Heinen, ley
 *
 */
public class Parser {
	private String xmlUrl;

	Parser(String uri) {
		xmlUrl = uri;
	}

	public void startParsing(String uri, ConfigHandler handler){
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		SAXParser parser;
		try {
			/*
			 * Initialize the SAXParser
			 */
			parser = parserFactory.newSAXParser();
			File file = new File(uri);

			/*
			 * Initialize the filewriter for output
			 */
			InputStream inputStream= new FileInputStream(file);
			Reader reader = new InputStreamReader(inputStream,"ISO-8859-1");
			InputSource is = new InputSource(reader);

			/*
			 * create an input stream to enforce the SAX parser using "ISO-8859-1" encoding
			 */
			is.setEncoding("ISO-8859-1");
			/*
			 * Start Parsing!
			 */
			parser.parse(is, handler);
			parser.getXMLReader().setFeature(
					"http://xml.org/sax/features/validation", true);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String getXmlUrl() {
		return xmlUrl;
	}

	public void setXmlUrl(String xmlUrl) {
		this.xmlUrl = xmlUrl;
	}
}
