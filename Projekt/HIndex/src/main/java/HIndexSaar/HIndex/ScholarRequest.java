package HIndexSaar.HIndex;

import java.util.ArrayList;
import java.util.List;

public abstract class ScholarRequest {

	private String url;
	private String searchName;
	private List<String> results;


	public ScholarRequest(String url, String searchItem){
		this.url = url;
		searchName = searchItem;
		results = new ArrayList<String>();
	}
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<String> getResults() {
		return results;
	}

	public void setResults(List<String> results) {
		this.results = results;
	}
	public String getSearchName() {
		return searchName;
	}
	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}
}
